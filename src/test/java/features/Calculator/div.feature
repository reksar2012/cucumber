@testCalculator
Feature: Calculator function Div
  Scenario: Div 2 positive number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Input with id "result" should have value "1"

  Scenario: Div 1 positive and 1 negative number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "-2"
    And button Div Click
    Then Input with id "result" should have value "-0.5"

  Scenario: Div 1 positive number and zero
    Given open calculator
    When Input with id "firstNumber" set value "0"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Input with id "result" should have value "0"

  Scenario: Div 2 negative number
    Given open calculator
    When Input with id "firstNumber" set value "-1"
    And Input with id "secondNumber" set value "-2"
    And button Div Click
    Then Input with id "result" should have value "0.5"

  Scenario: Div 1 number and 1 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2"
    And button Div Click
    Then Input with id "result" should have value "0.6"

  Scenario: Div 2 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2.4"
    And button Div Click
    Then Input with id "result" should have value "0.5"

  Scenario: Div float with zero
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "0"
    And button Div Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Div number with empty
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value ""
    And button Div Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Div 2 empty
    Given open calculator
    When Input with id "firstNumber" set value ""
    And Input with id "secondNumber" set value ""
    And button Div Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Div letter  and number with number
    Given open calculator
    When Input with id "firstNumber" set value "e.2"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Get Allert with text "Ошибка"

  Scenario: Div spec letter
    Given open calculator
    When Input with id "firstNumber" set value "@"
    And Input with id "secondNumber" set value "2"
    And button Div Click
    Then Get Allert with text "Ошибка"

  Scenario: Div 2 zero
    Given open calculator
    When Input with id "firstNumber" set value "0"
    And Input with id "secondNumber" set value "0"
    And button Div Click
    Then Get Allert with text "Невозможно вычеслить"
  Scenario: Div NaN
    Given open calculator
    When Input with id "firstNumber" set value "NaN"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Get Allert with text "Ошибка"
  Scenario: Div null
    Given open calculator
    When Input with id "firstNumber" set value "null"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Get Allert with text "Ошибка"
  Scenario: Div undefined
    Given open calculator
    When Input with id "firstNumber" set value "undefined"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Get Allert with text "Ошибка"
  Scenario: Div Infinity
    Given open calculator
    When Input with id "firstNumber" set value "Infinity"
    And Input with id "secondNumber" set value "1"
    And button Div Click
    Then Get Allert with text "Невозможно вычеслить"

    