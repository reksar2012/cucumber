package Runer;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
@CucumberOptions(
        features = "src/test/java/features",
        tags = "@test"
)
public class test extends AbstractTestNGCucumberTests {
}