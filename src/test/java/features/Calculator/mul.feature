@testCalculator
Feature: Calculator function mul
  Scenario: mul 2 positive number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Input with id "result" should have value "1"

  Scenario: mul 1 positive and 1 negative number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "-2"
    And button Mul Click
    Then Input with id "result" should have value "-2"

  Scenario: Mul 1 positive number and zero
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "0"
    And button Mul Click
    Then Input with id "result" should have value "0"

  Scenario: Mul 2 negative number
    Given open calculator
    When Input with id "firstNumber" set value "-1"
    And Input with id "secondNumber" set value "-2"
    And button Mul Click
    Then Input with id "result" should have value "2"

  Scenario: Mul 1 number and 1 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2"
    And button Mul Click
    Then Input with id "result" should have value "2.4"

  Scenario: Mul 2 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2.3"
    And button Mul Click
    Then Input with id "result" should have value "2.76"

  Scenario: Mul number with empty
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value ""
    And button Mul Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Mul 2 empty
    Given open calculator
    When Input with id "firstNumber" set value ""
    And Input with id "secondNumber" set value ""
    And button Mul Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Mul letter  and number with number
    Given open calculator
    When Input with id "firstNumber" set value "e.2"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Get Allert with text "Ошибка"

  Scenario: Mul spec letter
    Given open calculator
    When Input with id "firstNumber" set value "@"
    And Input with id "secondNumber" set value "2"
    And button Mul Click
    Then Get Allert with text "Ошибка"


  Scenario: Mul NaN
    Given open calculator
    When Input with id "firstNumber" set value "NaN"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Get Allert with text "Ошибка"
  Scenario: Mul null
    Given open calculator
    When Input with id "firstNumber" set value "null"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Get Allert with text "Ошибка"
  Scenario: Mul undefined
    Given open calculator
    When Input with id "firstNumber" set value "undefined"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Get Allert with text "Ошибка"
  Scenario: Mul Infinity
    Given open calculator
    When Input with id "firstNumber" set value "Infinity"
    And Input with id "secondNumber" set value "1"
    And button Mul Click
    Then Get Allert with text "Невозможно вычеслить"