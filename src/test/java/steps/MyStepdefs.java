package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class MyStepdefs {
    int TimeOut = 100;
    SelenideElement firstNumber = $(By.id("firstNumber"));
    SelenideElement secondNumber = $(By.id("secondNumber"));
    SelenideElement addButton = $(By.id("+"));
    SelenideElement subButton = $(By.id("-"));
    SelenideElement divButton = $(By.id("/"));
    SelenideElement mulButton = $(By.id("*"));
    SelenideElement result = $(By.id("result"));
    ;

    public MyStepdefs() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        Configuration.browser = "chrome";
    }

    @Given("^open calculator$")
    public void open() {
        Selenide.open("file:///C:/Users/reksar2012/IdeaProjects/SelenidCucumber/src/main/resources/Calculator.html");
        sleep(TimeOut);
    }

    @When("^Input with id \"([^\"]*)\" set value \"([^\"]*)\"$")
    public void inputWithIdSetValue(String id, String value) throws Throwable {
        $(By.id(id)).setValue(value);
        sleep(TimeOut);

    }

    @Then("^Input with id \"([^\"]*)\" should have value \"([^\"]*)\"$")
    public void inputWithIdShouldHaveValue(String id, String result) {
        $(By.id(id)).shouldHave(Condition.value(result));
    }

    @And("^button Add Click$")
    public void buttonAddClick() {
        addButton.click();
        sleep(TimeOut);
    }

    @And("^button Sub Click$")
    public void buttonSubClick() {
        subButton.click();
        sleep(TimeOut);
    }

    @And("^button Div Click$")
    public void buttonDivClick() {
        divButton.click();
        sleep(TimeOut);
    }

    @And("^button Mul Click$")
    public void buttonMulClick() {
        mulButton.click();
        sleep(TimeOut);
    }

    @Then("^Get Allert with text \"([^\"]*)\"$")
    public void getAllertWithText(String targetResult) throws Throwable {
        String result = Selenide.switchTo().alert().getText();
        Selenide.switchTo().alert().accept();
        Assert.assertEquals(result, targetResult);

    }
}
