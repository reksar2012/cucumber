@testCalculator
Feature: Calculator function sub
  Scenario: sub 2 positive number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Input with id "result" should have value "0"

  Scenario: sub 1 positive and 1 negative number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "-2"
    And button Sub Click
    Then Input with id "result" should have value "3"

  Scenario: sub 1 positive number and zero
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "0"
    And button Sub Click
    Then Input with id "result" should have value "1"

  Scenario: sub 2 negative number
    Given open calculator
    When Input with id "firstNumber" set value "-1"
    And Input with id "secondNumber" set value "-2"
    And button Sub Click
    Then Input with id "result" should have value "1"

  Scenario: sub 1 number and 1 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2"
    And button Sub Click
    Then Input with id "result" should have value "-0.8"

  Scenario: sub 2 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2.3"
    And button Sub Click
    Then Input with id "result" should have value "-1.1"
  Scenario: Sub number with empty
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value ""
    And button Sub Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Sub 2 empty
    Given open calculator
    When Input with id "firstNumber" set value ""
    And Input with id "secondNumber" set value ""
    And button Sub Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: Sub letter  and number with number
    Given open calculator
    When Input with id "firstNumber" set value "e.2"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Get Allert with text "Ошибка"

  Scenario: Sub spec letter
    Given open calculator
    When Input with id "firstNumber" set value "@"
    And Input with id "secondNumber" set value "2"
    And button Sub Click
    Then Get Allert with text "Ошибка"


  Scenario: Sub NaN
    Given open calculator
    When Input with id "firstNumber" set value "NaN"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Get Allert with text "Ошибка"
  Scenario: Sub null
    Given open calculator
    When Input with id "firstNumber" set value "null"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Get Allert with text "Ошибка"
  Scenario: Sub undefined
    Given open calculator
    When Input with id "firstNumber" set value "undefined"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Get Allert with text "Ошибка"
  Scenario: Sub Infinity
    Given open calculator
    When Input with id "firstNumber" set value "Infinity"
    And Input with id "secondNumber" set value "1"
    And button Sub Click
    Then Get Allert with text "Невозможно вычеслить"
    