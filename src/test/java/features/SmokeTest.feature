@test
Feature: smoke test #1, go through the service to Yandex-pay-page

  Scenario: go through the service to button "Купить"

    #actions at first page
    Given open riskmarket.ru
    When press button with text "Войти"
    And type to input with name "userName" text: "Stenchev60@gmail.com"
    And type to input with name "password" text: "ViCCzKveaokMpTA"
    And press element with value "Войти"
    And wait until login frame disappears
    And select countries: Шенген, Финляндия, Китай
    And specify birthday of tourists: 08.12.1945
    And specify dates of journey, any available dates
    And press button with text "Рассчитать полис"
    #actions at second page
    And wait until spinner disappears
    Then element with tag "search-result-item" should exist

