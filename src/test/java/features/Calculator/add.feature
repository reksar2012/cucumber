@testCalculator
Feature: Calculator function add
  Scenario: add 2 positive number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Input with id "result" should have value "2"

  Scenario: add 1 positive and 1 negative number
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "-2"
    And button Add Click
    Then Input with id "result" should have value "-1"

  Scenario: add 1 positive number and zero
    Given open calculator
    When Input with id "firstNumber" set value "1"
    And Input with id "secondNumber" set value "0"
    And button Add Click
    Then Input with id "result" should have value "1"

  Scenario: add 2 negative number
    Given open calculator
    When Input with id "firstNumber" set value "-1"
    And Input with id "secondNumber" set value "-2"
    And button Add Click
    Then Input with id "result" should have value "-3"

  Scenario: add 1 number and 1 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2"
    And button Add Click
    Then Input with id "result" should have value "3.2"

  Scenario: add 2 float
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value "2.3"
    And button Add Click
    Then Input with id "result" should have value "3.5"

  Scenario: add number with empty
    Given open calculator
    When Input with id "firstNumber" set value "1.2"
    And Input with id "secondNumber" set value ""
    And button Add Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: add 2 empty
    Given open calculator
    When Input with id "firstNumber" set value ""
    And Input with id "secondNumber" set value ""
    And button Add Click
    Then Get Allert with text "Невозможно вычеслить"

  Scenario: add letter  and number with number
    Given open calculator
    When Input with id "firstNumber" set value "e.2"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Get Allert with text "Ошибка"

  Scenario: add spec letter
    Given open calculator
    When Input with id "firstNumber" set value "@"
    And Input with id "secondNumber" set value "2"
    And button Add Click
    Then Get Allert with text "Ошибка"


  Scenario: Add NaN
    Given open calculator
    When Input with id "firstNumber" set value "NaN"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Get Allert with text "Ошибка"
  Scenario: Add null
    Given open calculator
    When Input with id "firstNumber" set value "null"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Get Allert with text "Ошибка"
  Scenario: Add undefined
    Given open calculator
    When Input with id "firstNumber" set value "undefined"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Get Allert with text "Ошибка"
  Scenario: Add Infinity
    Given open calculator
    When Input with id "firstNumber" set value "Infinity"
    And Input with id "secondNumber" set value "1"
    And button Add Click
    Then Get Allert with text "Невозможно вычеслить"
    